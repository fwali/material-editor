#pragma once

#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_


enum BUFFERS
{
	VAO,
	IBO,
	VBO,
	TEX_BUFFER,
	NORMAL_BUFFER,
	TANGENT_BUFFER,
	BITANGENT_BUFFER,


	NUM_BUFFERS
};

class Geometry
{
public:
	Geometry();

	void init();
	void render();


private:

	unsigned int buffers[NUM_BUFFERS];
	int numVerts;
	int numIndices;
};



#endif