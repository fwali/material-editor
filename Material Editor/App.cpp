#include "App.h"
#include <iostream>
#include <sstream>
#include <cstdio>

#include "Engine/Shader.h"

using namespace MaterialEditor;

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
	{
		return false;
	}

	std::stringstream redirectStream;
	std::cout.rdbuf(redirectStream.rdbuf());
	std::cerr.rdbuf(redirectStream.rdbuf());
	std::string consoleOutput;
	


	new Frame("Material Editor");

	consoleOutput = redirectStream.str();

	wxMessageBox(wxString(consoleOutput), "Console output");

	return true;
}

int MyApp::OnExit()
{

	return wxApp::OnExit();
}

IMPLEMENT_APP(MyApp)
// ----------------------------------------------------------------------------
// Frame: main application window
// ----------------------------------------------------------------------------
#define wx_SCROLL 20
wxBEGIN_EVENT_TABLE(Frame, wxFrame)
EVT_MENU(wxID_NEW, Frame::OnNewWindow)
EVT_MENU(wxID_CLOSE, Frame::OnClose)
EVT_SLIDER(_SLIDER, Frame::OnScroll)
EVT_COLOURPICKER_CHANGED(_SUNLIGHT_COLOUR_CHANGED, Frame::onSunlightColourChanged)
EVT_COLOURPICKER_CHANGED(_MATERIAL_COLOUR_CHANGED, Frame::onMaterialColourChanged)
EVT_COLOURPICKER_CHANGED(_AMBIENT_COLOUR_CHANGED, Frame::onAmbientColourChanged)
wxEND_EVENT_TABLE()


void Frame::onSunlightColourChanged(wxColourPickerEvent & WXUNUSED(event))
{
	this->sunlightColour = glm::vec4(this->sunlightColourParam->getColourVector(), 1.0f);
}

void Frame::onMaterialColourChanged(wxColourPickerEvent & WXUNUSED(event))
{
	this->materialColour = glm::vec4(this->materialColourParam->getColourVector(), 1.0f);
}

void Frame::onAmbientColourChanged(wxColourPickerEvent & WXUNUSED(event))
{
	this->ambientColour = glm::vec4(this->ambientLightColourParam->getColourVector(), 1.0f);
}

// update all scrolling events here
void Frame::OnScroll(wxCommandEvent & WXUNUSED(event) )
{
	this->materialColourIntensity = materialIntensityParam->getValueAsFP();
	this->roughness = roughnessParam->getValueAsFP();
	this->specularColour = specularColourParam->getValueAsFP();

	this->sunlightIntensity = sunlightIntensityParam->getValueAsFP();
	this->ambientIntensity = ambientIntensityParam->getValueAsFP();

	this->materialIntensityParam->updateTextbox();
	this->roughnessParam->updateTextbox();
	this->specularColourParam->updateTextbox();

	this->sunlightIntensityParam->updateTextbox();
	this->ambientIntensityParam->updateTextbox();
}

void Frame::setupData()
{
	specularColour = 0.4f;
	roughness = 0.7f;
	sunlightDir = glm::normalize(glm::vec4(-1.0f, -0.5f, 1.0f, 0.0f));
	sunlightColour = glm::vec4(0.75f, 0.749f, 0.678f, 0.0f);
	sunlightIntensity = 0.6f;

	ambientColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	ambientIntensity = 0.3f;

	materialColour = glm::vec4(1.00f, 0.766f, 0.366f, 1.0f);	// gold colour
	materialColourIntensity = 0.0f;

}

Frame::Frame(wxString windowName) : wxFrame(NULL, wxID_ANY, windowName)
{
	brightness = 1.0f;
	blue = 0.0f;
	this->windowName = windowName;

	this->glCanvas = new GLCanvas(this, 400, 400, NULL);

	setupData();

	wxMenu *menu = new wxMenu;
	menu->Append(wxID_NEW);
	menu->AppendSeparator();
	menu->Append(wxID_CLOSE);

	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append(menu, _T("&File"));

	this->bookCtrl = new wxBookCtrl(this, _BOOK_CONTROL);

	this->bookCtrl->SetPosition(wxPoint(430, 20));
	this->bookCtrl->SetSize(wxSize(350, 570));
	this->bookCtrl->SetupColours();

	this->panelMaterialParams = new wxPanel(this->bookCtrl);
	this->panelLightParams = new wxPanel(this->bookCtrl);

	this->bookCtrl->AddPage(panelMaterialParams, wxT("Parameters"), true);
	this->bookCtrl->AddPage(panelLightParams, wxT("Light"), false);
	
	materialColourParam = new ColourSelectorWidget(panelMaterialParams, _MATERIAL_COLOUR_CHANGED, "Material Colour", 30, 40, glm::vec3(materialColour));
	materialIntensityParam = new ShaderParamWidget(panelMaterialParams, _SLIDER, _TEXTBOX, "Material Intensity", 10, 30, 80);
	roughnessParam = new ShaderParamWidget(panelMaterialParams, _SLIDER, _TEXTBOX, "Roughness", 60, 30, 140);
	specularColourParam = new ShaderParamWidget(panelMaterialParams, _SLIDER, _TEXTBOX, "Specular Colour", 50, 30, 190);

	sunlightColourParam = new ColourSelectorWidget(panelLightParams, _SUNLIGHT_COLOUR_CHANGED, "Sunlight Colour", 30, 40, glm::vec3(0.75f, 0.749f, 0.678f));
	sunlightIntensityParam = new ShaderParamWidget(panelLightParams, _SLIDER, _TEXTBOX, "Sunlight Intensity", this->sunlightIntensity*100 ,30, 80);

	ambientLightColourParam = new ColourSelectorWidget(panelLightParams, _AMBIENT_COLOUR_CHANGED, "Ambient Colour", 30, 160, glm::vec3(this->ambientColour));
	ambientIntensityParam = new ShaderParamWidget(panelLightParams, _SLIDER, _TEXTBOX, "Ambient Intensity", this->ambientIntensity * 100, 30, 200);

	this->setupUniforms();

	SetMenuBar(menuBar);

	CreateStatusBar();

	SetClientSize(800, 600);
	Show();
}

void Frame::setupUniforms()
{
	Shader * glShader = this->glCanvas->getShader();
	glShader->loadUniform("specularColour", NSUniform::FLOAT, &specularColour);
	glShader->loadUniform("m", NSUniform::FLOAT, &roughness);
	glShader->loadUniform("sunDir", NSUniform::VEC4f, &sunlightDir);
	glShader->loadUniform("sunColour", NSUniform::VEC4f, &sunlightColour);
	glShader->loadUniform("sunIntensity", NSUniform::FLOAT, &sunlightIntensity);
	glShader->loadUniform("ambientColour", NSUniform::VEC4f, &ambientColour);
	glShader->loadUniform("ambientIntensity", NSUniform::FLOAT, &ambientIntensity);
	glShader->loadUniform("materialColour", NSUniform::VEC4f, &materialColour);
	glShader->loadUniform("materialColourIntensity", NSUniform::FLOAT, &materialColourIntensity);
}

void Frame::OnClose(wxCommandEvent& WXUNUSED(event))
{
	Close(true);
}

void Frame::OnNewWindow(wxCommandEvent& WXUNUSED(event))
{

}