
#include <GL/glew.h>

#include "global.h"
#include "GL.h"
#include "Engine/shader.h"

#include <IL/il.h>

#include <wx/wx.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <sstream>


// ----------------------------------------------------------------------------
// GLContext
// ----------------------------------------------------------------------------

using namespace NSUniform;

GLContext::GLContext(wxGLCanvas * canvas, int viewportWidth, int viewportHeight) : wxGLContext(canvas)
{
	SetCurrent(*canvas);

	this->viewportWidth = viewportWidth;
	this->viewportHeight = viewportHeight;

	GLenum error = glewInit();

	if (error != GLEW_OK)
	{
		//wxString output;
		//printf("ERROR: %s\n", glewGetErrorString(error));
		std::cout << "Error initializing Glew\n";
		//wxMessageBox(output);
	}
	else
	{
		std::cout<< "GLEW initialised\n";
	}

	ilInit();

	this->brightness = 0.1f;
	this->worldMatrix = glm::mat4(1.0f);
	this->viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.f, 4.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	this->projectionMatrix = glm::perspective(70.0f, 1.0f, 0.1f, 90.0f);
	

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// OpenGL initialisation here
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	this->shader = new Shader("normal.vert.glsl", "normal.frag.glsl");

	
	shader->loadUniform("viewMatrix", UT::MATRIX4f, glm::value_ptr(this->viewMatrix));
	shader->loadUniform("projectionMatrix", UT::MATRIX4f, glm::value_ptr(this->projectionMatrix));
	shader->loadUniform("worldMatrix", UT::MATRIX4f, glm::value_ptr(this->worldMatrix));

	this->albedoMap.load(GL_TEXTURE_2D, "Textures/brick_albedo.jpg");
	this->normalMap.load(GL_TEXTURE_2D, "Textures/brick_normal.jpg");

	int unit = 0;
	glActiveTexture(GL_TEXTURE0  + unit);
	this->albedoMap.bind();
	shader->loadUniform("albedo", UT::INT, &unit);
	/*
	int i2 = 1;
	glActiveTexture(GL_TEXTURE0 + i2);
	this->normalMap.bind();
	shader->loadUniform("normalMap", UT::INT, &i2);
	*/
	this->geometry.init();
	this->shader->bind();
	
}

glm::mat4 GLContext::getWorldMatrix()
{
	return this->worldMatrix;
}

void GLContext::rotate(glm::vec3 axis, float angle)
{
	glm::mat4 rotationMatrix = glm::rotate(angle, axis);
	this->worldMatrix *= rotationMatrix;
}

Shader * GLContext::getShader()
{
	return this->shader;
}

void GLContext::render()
{
	//this->brightness += 0.0001f;
	int unit = 0;
	glActiveTexture(GL_TEXTURE0 + unit);
	this->albedoMap.bind();
	shader->loadUniform("albedo", UT::INT, &unit);
	
	int i2 = 1;
	glActiveTexture(GL_TEXTURE1);
	this->normalMap.bind();
	shader->loadUniform("normalMap", UT::INT, &i2);
	

	this->shader->bindUniforms();
	this->geometry.render();
}

// ----------------------------------------------------------------------------
// GLCanvas
// ----------------------------------------------------------------------------

wxBEGIN_EVENT_TABLE(GLCanvas, wxGLCanvas)
	EVT_PAINT(GLCanvas::OnPaint)
	EVT_KEY_DOWN(GLCanvas::OnKeyDown)
	EVT_IDLE(GLCanvas::OnIdle)
	EVT_LEFT_DOWN(GLCanvas::OnMouseDown)
	EVT_MOTION(GLCanvas::OnMouseMove)
	EVT_LEFT_UP(GLCanvas::OnMouseUp)
	EVT_LEAVE_WINDOW(GLCanvas::OnMouseLeave)
wxEND_EVENT_TABLE()

GLCanvas::GLCanvas(wxWindow* parent, int width, int height, int * attribList)
	: wxGLCanvas(parent, wxID_ANY, attribList,
				wxPoint(20,20), wxSize(width, height),
				wxFULL_REPAINT_ON_RESIZE)

{
	this->width = width;
	this->height = height;

	this->glContext = new GLContext(this, width, height);
	this->mouseIsDown = false;

}

void GLCanvas::OnMouseMove(wxMouseEvent & event)
{
	if (!mouseIsDown)
		return;

	wxPoint currentMousePos = event.GetPosition();
	int x = currentMousePos.x;
	int y = this->height - currentMousePos.y;

	wxPoint mousePos = wxPoint(x, y);
	
	// do whatever work you need to do
	if (mousePos.x != lastMousePos.x || mousePos.y != lastMousePos.y)
		this->arcballRotate(mousePos);

	// update last position
	lastMousePos = mousePos;

	//wxMessageBox("FAS");
}

void GLCanvas::OnMouseUp(wxMouseEvent & WXUNUSED(event))
{
	this->mouseIsDown = false;
	
}

// When the mouse leaves the window
void GLCanvas::OnMouseLeave(wxMouseEvent & WXUNUSED(event))
{
	this->mouseIsDown = false;
}

void GLCanvas::OnMouseDown(wxMouseEvent & event)
{
	wxPoint mousePos = event.GetPosition();
	int mouseX = mousePos.x;
	int mouseY = mousePos.y;

	int x = mouseX;
	int y = this->height - mouseY;

	this->downMousePos = wxPoint(x, y);
	this->lastMousePos = wxPoint(x, y);

	this->mouseIsDown = true;
}

// enables scene to be redrawn
void GLCanvas::OnIdle(wxIdleEvent & WXUNUSED(event))
{
	Refresh(false);
}

Shader * GLCanvas::getShader()
{
	return this->glContext->getShader();
}

void GLCanvas::OnPaint(wxPaintEvent& WXUNUSED(event))
{
	wxPaintDC dc(this);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	this->glContext->render();

	SwapBuffers();

}

void GLCanvas::OnKeyDown(wxKeyEvent & WXUNUSED(event))
{
}

//-------------------------
// Arcball Rotation crap
//-------------------------

glm::vec3 GLCanvas::getArcballVector(wxPoint p)
{
	glm::vec3 P = glm::vec3(1.0*p.x / this->width*2.0 - 1.0,
							1.0*(this->height-p.y) / this->height*2.0 - 1.0,
							0);

	P.y = -1.0f*P.y;
	float p2 = P.x*P.x + P.y*P.y;

	if (p2 <= 1.0f)
	{
		P.z = sqrt(1.0f - p2);
	}
	else
	{
		if (p2 != 0.0f)
			P = glm::normalize(P);
	}

	return P;
}

void GLCanvas::arcballRotate(wxPoint mousePos)
{
	glm::vec3 va = getArcballVector(lastMousePos);
	glm::vec3 vb = getArcballVector(mousePos);
	float angle = acos(glm::min(1.0f, glm::dot(va, vb)));
	glm::vec3 axis = glm::cross(va, vb);

	this->glContext->rotate(axis, glm::degrees(angle));
}