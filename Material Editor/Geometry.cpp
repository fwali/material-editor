#include "Geometry.h"

#include <GL/glew.h>

Geometry::Geometry()
{
	for (unsigned int i = 0; i < NUM_BUFFERS; i++)
	{
		this->buffers[i] = 0;
	}
}

struct _Vertex
{
	float p[3];
};

struct _Texcoord
{
	float uv[2];
};

typedef unsigned int uint;

void Geometry::init()
{
	const int numVerts = 24;// 4;
	const int numIndices =  24;// 6;

	this->numVerts = numVerts;
	this->numIndices = numIndices;

	_Vertex vertices[numVerts] = { // Front Face
		{ -1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f },
		{ 1.0f, -1.0f, 1.0f }, { -1.0f, -1.0f, 1.0f },
		// Left Face
		{ -1.0f, 1.0f, -1.0f }, { -1.0f, 1.0f, 1.0f },
		{ -1.0f, -1.0f, 1.0f}, { -1.0f, -1.0f, -1.0f },
		// Back Face
		{ 1.0f, 1.0f, -1.0f }, { -1.0f, 1.0f, -1.0f },
		{ -1.0f, -1.0f, -1.0f }, { 1.0f, -1.0f, -1.0f },
		// Right Face
		{ 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, -1.0f },
		{ 1.0f, -1.0f, -1.0f }, { 1.0f, -1.0f, 1.0f },
		// Top Face
		{ -1.0f, 1.0f, -1.0f }, { 1.0f, 1.0f, -1.0f },
		{ 1.0f, 1.0f, 1.0f}, { -1.0f, 1.0f, 1.0f },
		// Bottom Face
		{ -1.0f, -1.0f, 1.0f }, { 1.0f, -1.0f, 1.0f },
		{ 1.0f, -1.0f, -1.0f }, { -1.0f, -1.0f, -1.0f }
	};

	_Texcoord texcoords[numVerts] = {
		// Front Face
		{ 0.0f, 1.0f }, { 1.0f, 1.0f },
		{ 1.0f, 0.0f }, {0.0f, 0.0f },
		// Left Face
		{ 0.0f, 1.0f }, { 1.0f, 1.0f },
		{ 1.0f, 0.0f }, { 0.0f, 0.0f },
		// Back Face
		{ 0.0f, 1.0f }, { 1.0f, 1.0f },
		{ 1.0f, 0.0f }, { 0.0f, 0.0f },
		// Right Face
		{ 0.0f, 1.0f }, { 1.0f, 1.0f },
		{ 1.0f, 0.0f }, { 0.0f, 0.0f },
		// Top Face
		{ 0.0f, 1.0f }, { 1.0f, 1.0f },
		{ 1.0f, 0.0f }, { 0.0f, 0.0f },
		// Bottom Face
		{ 0.0f, 1.0f }, { 1.0f, 1.0f },
		{ 1.0f, 0.0f }, { 0.0f, 0.0f },
	};

	// Tangent space matrix is [Tangents, Bitangents, Normal]

	_Vertex normals[numVerts] =
	{
		// Front Face
		{ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f },
		// Left Face
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f },
		// Back Face
		{ 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f, -1.0f },
		// Right Face
		{ 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f },
		// Top Face
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		// Bottom Face
		{ 0.0f, -1.0f, 0.0f }, { 0.0f, -1.0f, 0.0f },
		{ 0.0f, -1.0f, 0.0f, }, {0.0f, -1.0f, 0.0f }
	};

	_Vertex tangents[numVerts] =
	{
		// Front Face
		{ 1.0f, 1.0f, 0.0f }, { 1.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f },
		// Left Face
		{ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f },
		// Back Face
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f },
		 // Right Face
		{ 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f, -1.0f },
		// Top Face
		{ 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f },
		// Bottom Face
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f }
	};

	_Vertex bitangents[numVerts] =
	{
		// Front Face
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		// Left Face
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		// Back Face
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		// Right Face
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		// Top Face
		{ 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f, -1.0f },
		// Bottom Face
		{ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f },
	};

	glGenVertexArrays(1, &buffers[VAO]);
	glBindVertexArray(buffers[VAO]);

	glGenBuffers(NUM_BUFFERS-1, &buffers[1]);
	
	// Create the IBO (Element array buffer)
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->buffers[IBO]);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(uint), &indices[0], GL_STATIC_DRAW);

	// Create the vertex buffer object
	glBindBuffer(GL_ARRAY_BUFFER, this->buffers[VBO]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_Vertex)*numVerts, &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, 0, 0, 0);

	// Create the normal buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->buffers[NORMAL_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_Vertex)*numVerts, &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, 0, 0, 0);

	// UV-coordinate buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->buffers[TEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_Texcoord)*numVerts, &texcoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, 0, 0, 0);

	// Tangent buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->buffers[TANGENT_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_Vertex)*numVerts, &tangents[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, 0, 0, 0);

	// Bitangent buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->buffers[BITANGENT_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_Vertex)*numVerts, &bitangents[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, 0, 0, 0);


	// Reset buffers in OpenGL
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Geometry::render()
{
	glBindVertexArray(buffers[VAO]);

	//glDrawElements(GL_QUADS, this->numIndices, GL_UNSIGNED_INT, 0);
	glDrawArrays(GL_QUADS, 0, 24);

	glBindVertexArray(0);

}
