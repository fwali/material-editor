#pragma once

#ifndef _MATERIAL_EDITOR_APP_
#define _MATERIAL_EDITOR_APP_

#include <wx/wx.h>
#include <wx/bookctrl.h>

#include "Helper.h"

#include "GL.h"

#include <glm/glm.hpp>

using namespace Helper;

namespace MaterialEditor
{

	class MyApp : public wxApp
	{
	public:
		MyApp() { };

		virtual bool OnInit();
		virtual int OnExit();

	private:
	};

	enum Actions
	{
		_NEW,
		_EXIT,
		_SLIDER,
		_TEXTBOX,
		_BOOK_CONTROL,
		_SUNLIGHT_COLOUR_CHANGED,
		_MATERIAL_COLOUR_CHANGED,
		_AMBIENT_COLOUR_CHANGED


		,NUM_ACTIONS
	};

	class Frame : public wxFrame
	{
	public:
		Frame(wxString windowName);

	private:
		void OnClose(wxCommandEvent & event);
		void OnNewWindow(wxCommandEvent & event);

		void OnScroll(wxCommandEvent & event);

		void setupUniforms();
		void setupData();

		/*
			Colour Change Events
		*/
		void onSunlightColourChanged(wxColourPickerEvent & event);
		void onMaterialColourChanged(wxColourPickerEvent & event);
		void onAmbientColourChanged(wxColourPickerEvent & event);

		float brightness;
		float blue;

		/*
				Shader Params here
		*/
		glm::vec4 sunlightColour;
		glm::vec4 sunlightDir;
		glm::vec4 ambientColour;

		glm::vec4 materialColour;
		float materialColourIntensity;

		float ambientIntensity;
		float roughness;
		float specularColour;
		float sunlightIntensity;

		wxPanel * panelMaterialParams;
		wxPanel * panelLightParams;

		ShaderParamWidget * materialIntensityParam;
		ShaderParamWidget * roughnessParam;
		ShaderParamWidget * specularColourParam;

		ShaderParamWidget * sunlightIntensityParam;
		ShaderParamWidget * ambientIntensityParam;

		ColourSelectorWidget * ambientLightColourParam;
		ColourSelectorWidget * sunlightColourParam;
		ColourSelectorWidget * materialColourParam;

		wxBookCtrl * bookCtrl;

		wxString windowName;
		GLCanvas *glCanvas;

		wxDECLARE_EVENT_TABLE();
	};

};

#endif