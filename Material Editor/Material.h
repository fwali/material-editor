#pragma once

#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include <string>

#include <glm/glm.hpp>

/*
		These are just parameters that run through our lighting algorithms
		and decides how a certain mesh looks.

		Materials are allowed to "inherit" other materials.
*/

class Material
{
public:
	Material();
	Material(Material * parent, std::string name, glm::vec4 colour, float specularColour, float roughness, std::string diffuseMap, std::string normalMap);

	void loadParameters();	// crawl through the Material hierarchy assigning the appropriate properties (load the right textures here)

	void setParent(Material * p);
	void setColour(glm::vec4 colour);
	void setSpecularColour(float sc);
	void setRoughness(float m);

	float* getSpecColourPtr();
	float* getRoughnessPtr();
	float* getDiffuseColourPtr();

private:
	Material * parent;

	std::string name;
	std::string diffuseMap;
	std::string normalMap;	// for later

	glm::vec4 diffuseColour;

	// actual material properties
	float specularColour;
	float roughness;

	bool inheritDiffuseTexture, inheritNormalTexture,
		inheritDiffuseColour, inheritSpecColour, inheritRoughness;
};


#endif