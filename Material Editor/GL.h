#pragma once

#ifndef _MATERIAL_EDITOR_GL_H_
#define _MATERIAL_EDITOR_GL_H_

#include <wx/glcanvas.h>
#include <glm/glm.hpp>

#include "Engine/Shader.h"
#include "Engine/Texture.h"

#include "Geometry.h"



using namespace NSUniform;

class GLContext : wxGLContext
{
public:
	GLContext(wxGLCanvas * canvas, int viewportWidth, int viewportHeight);

	void render();
	Shader * getShader();

	void rotate(glm::vec3 axis, float angle);
	glm::mat4 getWorldMatrix();

private:

	/*
			Shader params
	*/
	glm::mat4 worldMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	float brightness;

	// other properties
	int viewportWidth, viewportHeight;

	Shader* shader;
	Texture albedoMap;
	Texture normalMap;
	// need to bring in diffuse map
	GLuint textureID;
	Geometry geometry;
};

class GLCanvas : wxGLCanvas
{
public:
	GLCanvas(wxWindow * parent, int width, int height, int * attribList = NULL);

	Shader * getShader();

private:

	void OnKeyDown(wxKeyEvent& event);
	void OnPaint(wxPaintEvent& event);
	void OnIdle(wxIdleEvent& event);

	void OnMouseDown(wxMouseEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	void OnMouseUp(wxMouseEvent& event);
	void OnMouseLeave(wxMouseEvent& event);

	wxPoint lastMousePos;
	wxPoint downMousePos;
	bool mouseIsDown;

	glm::vec3 getArcballVector(wxPoint p);
	void arcballRotate(wxPoint mousePos);

	GLContext * glContext;
	int width, height;	// size of the viewport

	wxDECLARE_EVENT_TABLE();
};

#endif