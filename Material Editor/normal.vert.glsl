#version 330


layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;


out vec4 Pos;
out vec4 _Normal;
out vec2 TexCoords;

out mat4 tangentSpace;
out mat4 invTangentSpace;

uniform mat4 worldMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;


void main()
{
	TexCoords = texcoords;
	vec4 Tangent = normalize( vec4(tangent, 0.0)*worldMatrix);
	vec4 Bitangent = normalize( vec4(bitangent, 0.0)*worldMatrix);
	_Normal =normalize( vec4(normal, 0.0)*worldMatrix);

	//tangentSpace = transpose(mat4(Tangent, Bitangent, _Normal, vec4(0.0)));
	tangentSpace = mat4(transpose(mat3( vec3(Tangent), vec3(Bitangent), _Normal)));
	invTangentSpace = transpose(tangentSpace);
	Pos = vec4(pos, 1.0)*worldMatrix;
	gl_Position = Pos*(viewMatrix * projectionMatrix);
}