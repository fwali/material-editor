#pragma once

#ifndef _HELPER_H_
#define _HELPER_H_

#include <wx/wx.h>
#include <wx/clrpicker.h>

#include <glm/glm.hpp>

#include <string>

namespace Helper
{

// function declarations
wxColor convertToWXColor(glm::vec3 colour);
glm::vec3 convertToColorVector(wxColour colour);

// Classes

class ShaderParamWidget
{
public:
	ShaderParamWidget(wxWindow* parent, int sliderEvent, int textboxEvent, std::string name, int currentValue, int x, int y);
	~ShaderParamWidget();

	void setValue(float f);

	void updateTextbox();
	float getValueAsFP();
private:


	const int minValue = 0, maxValue = 100;
	const int sliderWidth = 200, sliderHeight = 20;

	int x, y, currentValue;

	wxStaticText	* label;
	wxSlider		* slider;
	wxTextCtrl		* textEntry;

	std::string paramName;
};

class ColourSelectorWidget
{
public:
	ColourSelectorWidget(wxWindow* parent, int colourEvent, std::string name, int x, int y, glm::vec3 colour);

	glm::vec3 getColourVector();

	void promptColourSelection();

private:
	const int colourSelectorWidth = 98, colourSelectorHeight = 23;
	const int labelWidth = 120;

	int x, y;

	wxColour colour;

	wxColourPickerCtrl * colourPicker;

	std::string paramName;

	wxStaticText	* label;
	wxButton * button;
};
}

#endif _HELPER_H_