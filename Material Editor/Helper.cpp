#include "GL.h"

#include "Helper.h"
#include "global.h"

#include <wx/wx.h>

using namespace Helper;

// Helper functions

wxColor Helper::convertToWXColor(glm::vec3 colour)
{
	unsigned char rgb[3] = {
		(unsigned char)((int)(colour.x * 255)),
		(unsigned char)((int)(colour.y * 255)),
		(unsigned char)((int)(colour.z * 255))
	};

	return wxColour(rgb[0], rgb[1], rgb[2]);
}

glm::vec3 Helper::convertToColorVector(wxColour colour)
{
	unsigned char rgb[3] = { colour.Red(), colour.Green(), colour.Blue() };

	return glm::vec3(
						(float)((int)rgb[0]) / 255.0f,
						(float)((int)rgb[1]) / 255.0f,
						(float)((int)rgb[2]) / 255.0f
					);
}


//**************************************************
//	ShaderParamWidget implementation
//**************************************************

float ShaderParamWidget::getValueAsFP()
{
	float current = ((float)(slider->GetValue()-minValue)) / ((float)maxValue);

	return current;
}

void ShaderParamWidget::updateTextbox()
{
	this->textEntry->SetLabelText( std::to_string(this->getValueAsFP()) );
}

void ShaderParamWidget::setValue(float f)
{
	int val = (int)(f * maxValue + minValue);
	currentValue = val; 
	this->slider->SetValue(val);
	this->textEntry->SetLabelText(std::to_string(f));
}

ShaderParamWidget::ShaderParamWidget(wxWindow* parent, int sliderEvent, int textboxEvent, std::string name, int currentValue, int x, int y)
{
	this->paramName = name;
	this->currentValue = currentValue;
	this->x = x;
	this->y = y;

	int textlabelHeight = 25;

	this->label = new wxStaticText(parent, 19, wxString(paramName), wxPoint(x, y), wxSize(sliderWidth, textlabelHeight));
	this->slider = new wxSlider(parent, sliderEvent, currentValue, this->minValue, this->maxValue, wxPoint(x, y + textlabelHeight), wxSize(this->sliderWidth, this->sliderHeight), wxSL_AUTOTICKS);
	this->slider->SetTickFreq((maxValue - minValue) / 10);
	this->textEntry = new wxTextCtrl(parent, textboxEvent, std::to_string(this->getValueAsFP()), wxPoint(x + sliderWidth + 5, y + textlabelHeight), wxSize(50, 25));

	//setValue(currentValue);
}

//**************************************************
// Colour Selector Widget implementation
//**************************************************

ColourSelectorWidget::ColourSelectorWidget(wxWindow * parent, int colourEvent, std::string name, int x, int y, glm::vec3 colour)
{
	this->paramName = name;
	this->x = x;
	this->y = y;
	this->colour = convertToWXColor(colour);

	this->label = new wxStaticText(parent, 72, this->paramName, wxPoint(x, y), wxSize(this->labelWidth, 30));
	this->colourPicker = new wxColourPickerCtrl(parent, colourEvent, this->colour, wxPoint(x + this->labelWidth, y - 5), wxSize(colourSelectorWidth, colourSelectorHeight));
}

glm::vec3 ColourSelectorWidget::getColourVector()
{
	return convertToColorVector(this->colourPicker->GetColour());
}

void ColourSelectorWidget::promptColourSelection()
{
	//wxColorDialog dialog(this, NULL);
}