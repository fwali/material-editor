#pragma once

#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "GLResource.h"

class Texture : public GLResource
{
public:
	GLenum textureType;

	Texture() : GLResource()
	{
		textureType = 0;
	}

	Texture(GLenum textureType, std::string filename)
	{
		this->textureType = textureType;
		this->filename = filename;
		this->glID = 0;
	};

	bool load();

	bool load(GLenum textureType, std::string filename);

	void unload();

	void bind();
};

#endif