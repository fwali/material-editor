#ifndef GLEW_STATIC
	#define GLEW_STATIC
#endif

#include <GL/glew.h>

#include "Shader.h"

#include <fstream>
#include <string>
#include <iostream>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/any.hpp>

#include <wx/wx.h>

using namespace NSUniform;

Shader::Shader()
{
	this->glID = 0;
	this->filename = "";
}

Shader::Shader(std::string vertexFilename, std::string fragmentFilename)
{
	this->load(vertexFilename, fragmentFilename);
}

void Shader::bind()
{
	glUseProgram(this->glID);
	
}


void Shader::bindUniforms()
{
	if (this->uniformMap.size() == 0)	// if no uniforms are required (DOUBT IT), then just return
	{
		return;
	}
	else
	{
		std::string uniformName;
		uniformPointer uniform;
		for( auto kv : this->uniformMap)
		{
			uniformName = kv.first;
			uniform = kv.second;

			if (uniform.type == UT::FLOAT)
			{
				glUniform1fv(uniform.location, 1, (float*) uniform.data);
			}
			else if (uniform.type == UT::INT)
			{
				glUniform1iv(uniform.location, 1, (int*)uniform.data);
			}
			else if (uniform.type == UT::VEC3f)
			{
				glUniform3fv(uniform.location, 1, (float*)uniform.data);
			}
			
			else if (uniform.type == UT::VEC4f)
			{
				glUniform4fv(uniform.location, 1, (float*)uniform.data );
			}
			else if (uniform.type == UT::MATRIX4f)
			{
				glUniformMatrix4fv(uniform.location, 1, GL_TRUE, (float*)uniform.data );
			}
			else
			{
				std::cout <<uniform.type << " is not a supported uniform type (" <<uniformName <<")\n";
				wxMessageBox("[ERROR] Invalid uniform");
			}
			
		}
	}
}

bool Shader::loadUniform(std::string uniformName, UT uType, void* uniformData)
{
	bool success = true;

	int uniformLocation = glGetUniformLocation(this->glID, uniformName.c_str());

	
	if (uniformLocation == -1) // uniform does not exist
	{
		success = false;
	}
	else		// uniform exists, add it to Map if it isn't in the map already
	{
		uniformPointer uniform;
		uniform.type = uType;
		uniform.location =  uniformLocation;
		uniform.data =  uniformData;

		this->uniformMap[uniformName] = uniform;

		// Commented out because adding the above line now allows overriding of uniforms. Very valuable
		/*
		if (this->uniformMap.find(uniformName) == this->uniformMap.end())	// if uniform hasn't already been associated with the shader
		{
			this->uniformMap[uniformName] = uniform;	// store it in the map by its name
		}
		else
		{
			success = false;
		}
		*/
	}

	return success;
}

void Shader::load(std::string vertexFilename, std::string fragmentFilename)
{
	this->glID = 0;
	glID = glCreateProgram();

	this->filename = vertexFilename+fragmentFilename;

	// compile and load individual shaders into memory
	GLuint vertexShaderID = this->loadShader(vertexFilename.c_str(), GL_VERTEX_SHADER);
	GLuint fragmentShaderID = this->loadShader(fragmentFilename.c_str(), GL_FRAGMENT_SHADER);

	glLinkProgram(this->glID);

	// cleanup intermediate shader objects
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	glValidateProgram(this->glID);
}

GLuint Shader::loadShader(const char* filename, GLenum type)
{
	GLuint shaderID = glCreateShader(type);

	// read in source code
	std::string sourceCode = "";
	std::string line = "";
	std::ifstream myfile (filename);

	if (myfile.is_open())
	{
		while ( getline (myfile,line) )
		{
			sourceCode += line + "\n";
		}
		
		myfile.close();
	}
	else
	{
		std::cerr << "Could not find file : "  <<filename <<std::endl;
	}

	const char* codeCopy = sourceCode.c_str();
	const GLchar* p[1];
	p[0] = &codeCopy[0];
	GLint length[1] = { strlen(codeCopy) };

	glShaderSource(shaderID, 1, p, length);

	glCompileShader(shaderID);

	GLint success;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

	if (!success) {
		GLchar InfoLog[1024];
		glGetShaderInfoLog(shaderID, sizeof(InfoLog), NULL, InfoLog);
		fprintf(stderr, "[Shader Error] compiling shader type %d: '%s'\n", type, InfoLog);
		std::cout << "[Shader Error] compiling shader type " << type << ": " << InfoLog << "\n";
		return 0;
	}

	std::cout <<"[Shader] Successfully loaded " <<this->filename <<std::endl;

	glAttachShader(this->glID, shaderID);
	return shaderID;

}

int Shader::getUniformLocation(std::string uniformName)
{
	return glGetUniformLocation(this->glID, uniformName.c_str());
}