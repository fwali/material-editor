#include <GL/glew.h>

#include "Texture.h"

#include <IL\il.h>

#include <iostream>

bool Texture::load()
{
	unsigned int ilImageID;

	ilGenImages(1, &ilImageID);
	ilBindImage(ilImageID);

	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);

	ILboolean success = ilLoadImage((ILstring) this->filename.c_str());

	if (!success)
	{
		ilDeleteImages(1, &ilImageID);
		std::cout <<"Could not load texture - " <<this->filename <<std::endl;
		return false;
	}

	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

	glGenTextures(1, &(this->glID));
	glBindTexture(this->textureType, this->glID);
	glTexImage2D(this->textureType, 0, GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGBA, GL_UNSIGNED_BYTE, ilGetData());

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	ilDeleteImages(1, &ilImageID);

	std::cout <<"[Texture] Successfully loaded " <<this->filename <<std::endl;
	return true;
}

bool Texture::load(GLenum textureType, std::string filename)
{
	this->textureType = textureType;
	this->filename = filename;

	return (this->load());
}

void Texture::bind()
{
	glBindTexture(this->textureType, this->glID);
}