#pragma once

#ifndef _GLResource_H_
#define _GLResource_H_

#include <string>

#include <GLFW/glfw3.h>

class GLResource
{
public:
	GLuint glID;
	std::string filename;

	GLResource();

};

#endif