#pragma once

#ifndef _SHADER_H_
#define _SHADER_H_

#include <GLFW\glfw3.h>

#include <string>
#include "GLResource.h"
#include <boost/unordered_map.hpp>

namespace NSUniform
{
	enum UniformType
	{
		INT,
		FLOAT,
		VEC2i,
		VEC2f,
		VEC3i,
		VEC3f,
		VEC4i,
		VEC4f,
		MATRIX3f,
		MATRIX4f
	};

	typedef UniformType UT;	// just to make typing a little less annoying

	struct uniformPointer
	{
		UniformType type;
		int location;
		void* data;

		void clear()
		{
			delete data;
		}

	};

	typedef boost::unordered_map<std::string, uniformPointer> hashMap;

	class Shader : public GLResource
	{
	public:
		hashMap uniformMap;

		Shader();
		Shader(std::string vertexFilename, std::string fragmentFilename);
		GLuint loadShader(const char* filename, GLenum type);
		bool loadUniform(std::string uniformName, UniformType uType, void* uniformData);
		void load(std::string vertexFilename, std::string fragmentFilename);
		void bindUniforms();
		void bind();

		int getUniformLocation(std::string uniformName);
	};
}
#endif