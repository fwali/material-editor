#include "Material.h"

//#include <glm/gtc/type_ptr.hpp>

Material::Material()
{

}

Material::Material(Material * parent, std::string name, glm::vec4 colour, float specularColour,
	float roughness, std::string diffuseMap, std::string normalMap)
{
	this->parent = parent;
	this->name = name;
	this->diffuseMap = diffuseMap;
	this->normalMap = normalMap;

	this->diffuseColour = colour;
	this->specularColour = specularColour;
	this->roughness = roughness;
}

void Material::setParent(Material * p)
{
	this->parent = p;
}

void Material::setColour(glm::vec4 colour)
{
	this->diffuseColour = colour;
}

void Material::setSpecularColour(float sc)
{
	this->specularColour = sc;
}

void Material::setRoughness(float m)
{
	this->roughness = m;
}

float* Material::getSpecColourPtr()
{
	return &(this->specularColour);
}

float* Material::getRoughnessPtr()
{
	return &(this->roughness);
}

float* Material::getDiffuseColourPtr()
{
	return &diffuseColour[0];//glm::value_ptr(this->diffuseColour);
}

enum Parameters
{
	DIFFUSE,
	NORMAL,
	SPEC_COLOUR,
	ROUGHNESS,
	DIFFUSE_COLOUR,


	NUM_PARAMETERS
};

void Material::loadParameters()
{
	if (this->parent == NULL)
		return;

	Material * temp = this->parent;

	bool found[NUM_PARAMETERS];
	for (int i = 0; i < NUM_PARAMETERS; i++)
	{
		found[i] = false;
	}

	while (temp != NULL)
	{
		if (this->inheritRoughness && !temp->inheritRoughness && !found[ROUGHNESS])
		{
			found[ROUGHNESS] = true;

			this->roughness = temp->roughness;
		}

		if (this->inheritSpecColour && !temp->inheritSpecColour && !found[SPEC_COLOUR])
		{
			found[SPEC_COLOUR] = true;

			this->specularColour = temp->specularColour;
		}

		if (this->inheritDiffuseColour && !temp->inheritDiffuseColour && !found[DIFFUSE_COLOUR])
		{
			found[DIFFUSE_COLOUR] = true;

			this->diffuseColour = temp->diffuseColour;
		}

		if (this->inheritDiffuseTexture && !temp->inheritDiffuseTexture && !found[DIFFUSE])
		{
			found[DIFFUSE] = true;

			this->diffuseMap = temp->diffuseMap;
		}

		if (this->inheritNormalTexture && !temp->inheritNormalTexture && !found[NORMAL])
		{
			found[NORMAL] = true;

			this->normalMap = temp->normalMap;
		}

		temp = temp->parent;
	}
}