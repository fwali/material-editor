#pragma once

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include "Helper.h"

#include <string>

namespace EVENTS
{
	enum Events
	{
		NEW,
		SAVE,
		LOAD,
		ROTATE_MODEL,


		NUM_EVENTS
	};

}

namespace global
{
	//std::string texDirectory = "";
	//std::string materialsDirectory = "";			
}

#endif