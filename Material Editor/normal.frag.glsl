#version 330

#define PI			3.141593
#define PI_OVER_2	1.570796
#define PI_OVER_4	0.785398

in vec4 Pos;
in vec4 _Normal;
in vec2 TexCoords;

in mat4 tangentSpace;
in mat4 invTangentSpace;

uniform float specularColour;	// specular colour parameter
uniform float m;				// roughness parameter


uniform vec4 sunDir;			// direction of sunlight
uniform vec4 sunColour;
uniform float sunIntensity;

uniform vec4 ambientColour;
uniform float ambientIntensity;

uniform vec4 materialColour;
uniform float materialColourIntensity;

uniform sampler2D albedo;
uniform sampler2D normalMap;

vec4 Normal;

float Fresnel(vec4 L, vec4 H)
{
	// Use Schlick's appromixation for Fresnel calculations
	float HDotL =  clamp(dot(H,L), 0.0 ,1.0);
	float exponential = pow( (1-HDotL), 5.0);
	float fSchlick = specularColour + (1 - specularColour)*exponential;

	return  fSchlick;
}

/* Distribution functions */
 float  blinnPhongNormalisation(vec4 H)
{
	float shininess = (2.0/(m*m)) - 2.0;
	float norm =  (shininess + 2.0f)/(2.0*PI);
	float NDotH = clamp(dot(Normal, H), 0.0, 1.0);
	float blinnPhong = pow(NDotH, shininess);

	float D = norm*blinnPhong;

	return D;
}

 float beckmannDistribution(vec4 H)
{
	float NDotH = clamp(dot(Normal, H), 0.0, 1.0);
	float NDotH2 = NDotH*NDotH;
	float NDotH4 = NDotH2*NDotH2;

	float D;
	float m2 = m*m;

	D = exp( (NDotH2 - 1) / (m2*NDotH2) );
	D = D/ (PI*m2*NDotH4);
	
	return D;
}

float DistributionFunction(vec4 H)
{

	// Return the distribution function of your choice
	return blinnPhongNormalisation(H);
}

/* Visibility Function */

 float kskCookTorranceApprox(vec4 L, vec4 V, vec4 H)
{
	return 4.0/(dot(V+L, V+L));
}

 float schlickSmithShadowing(vec4 L, vec4 V, vec4 H)
{
	float a = m*sqrt(1.0/PI_OVER_2);
	float NDotL = clamp(dot(Normal, L), 0.0, 1.0);
	float NDotV = clamp(dot(Normal, V), 0.0, 1.0);
	
	float visibility = ((NDotL * (1-a)) + a)*((NDotV*(1-a)) + a);
	visibility = 1.0 / visibility;

	return visibility;
}

float VisibilityFunction(vec4 L, vec4 V, vec4 H)
{
	return kskCookTorranceApprox(L, V, H);
}

// BRDF
vec4 MicrofacetBRDF(vec4 L, vec4 V, vec4 LightColour)
{
	vec4 H = normalize(L+V);
	float NDotL = 1.0;//clamp(dot(Normal, L), 0.0, 1.0);
	return NDotL*PI_OVER_4*DistributionFunction(H)*Fresnel(L, H)*VisibilityFunction(L, V, H) * LightColour;	//for now
}

vec4 calculateLighting()
{
	vec4 colour;
	vec4 V = vec4(0.0, 0.0, 1.0, 0.0);

	float NDotSun = clamp(dot(Normal, -sunDir), 0.0, 1.0);

	colour = NDotSun*sunColour*sunIntensity + MicrofacetBRDF(-sunDir, V, sunColour);

	return colour;
}

void main()
{
	vec4 gamma = vec4(1.0/2.2);

	vec4 rawTexel = vec4(texture2D(albedo, TexCoords).rgb, 1.0);
	vec4 albedo  = 1.0*pow(rawTexel, gamma) + materialColour*materialColourIntensity;

	vec3 normalMapTexel = normalize(texture2D(normalMap, TexCoords).rgb*2.0 - 1.0);
	Normal =  vec4(normalMapTexel, 0.0)*tangentSpace;

	vec4 finalColour = albedo*(ambientColour*ambientIntensity) + calculateLighting();
	gl_FragColor = pow(finalColour, 1.0/gamma);
}